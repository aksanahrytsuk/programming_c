using System.Linq;
using System.IO;
using System;
namespace Lesson_5
{
  public class Viselitsa
  {
    static string hidden_word = Get_Word(); //получаем результат метода в переменную
    static char[] hidden_world_char = hidden_word.ToCharArray(); //спрятаное слово состоит из символов типа char
    static char[] guessed_word = Init_Secret(); //слово, которое угадываю заменяется звёздочками
    static int health = 7;


    public static void Check_Match(char key) //проверяет совпадение введенной буквы и спрятанной, если нет совпадения минус жизнь
    {
      bool match = false;
      for (int i = 0; i < hidden_word.Length; i++)
      {
        if (key == hidden_world_char[i])
        {
          guessed_word[i] = key;
          match = true;
        }
      }
      if (!match)
      {
        Console.WriteLine("не угадал букву");
        health--;
      }
    }
    static bool Check_Key(char key) //проверка на ввод только букв РА нижнего регистра
    {
      char[] allow_list = { 'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я'};
      foreach (char item in allow_list)
      {
        if (item == key)
        {
          return true;
        }
      }
      return false;
    }
    public static char Get_Key() // проверяет, чтобы введена была только одна буква
    {
      while (true)
      {
        char[] key = Console.ReadLine().ToCharArray(); //ввод
        if (key.Length <= 1 && Check_Key(key[0]))
        {
          return key[0];
        }
        else
        {
          Console.WriteLine("Ошибка!!! Введите только 1 РУССКУЮ МАЛЕНЬКУЮ букву.");
        }
      }
    }
    public static bool Check_Win() // сравнение отгадываемого слова и спрятанного
    {
        if (Enumerable.SequenceEqual(guessed_word,hidden_world_char))
        {
          Console.WriteLine("You're WIN!!!");
          return true;
        }
        if (health == 0)
        {
          Console.WriteLine("You're LOSE!!!");
          return true;
        }
        return false;
    }
    public static char[] Init_Secret() //метод, который заменяет буквы на звездочки, делает слово скрытым для игрока
    {
      char[] dash = new char[hidden_word.Length];
      for (int i = 0; i < hidden_word.Length; i++)
      {
        dash[i] = '*'; //каждый элемент массива [i] заменить "*" длина массива dash = длине слова
      }
      return dash;
    }
    public static string Get_Word()
    {
      string path_to_file = @"C:\Users\Оксана\source\repos\Lesson_5\Lesson_5\word_rus.txt"; //записать путь файла со словами в переменную path_to_file
      string[] wordList = File.ReadAllLines(path_to_file); //массив содержит все слова файла.
      Random random = new Random(); // создать экземпляр рандома
      return wordList[random.Next(0, wordList.Length)]; //загаданное слово
    }
    public static void Main() //главный цикл игры
    {
      //Console.WriteLine($"Ваше загаданное слово {hidden_word}\n"); //debaging
      Console.WriteLine($"Ваше загаданное слово {new string(guessed_word)}"); //to string tipe

      while (!Check_Win())
      {
        Console.WriteLine($"Жизней: {health}"); //to string tipe
        Console.WriteLine($"Введите букву");
        char letter = Get_Key(); // приведение к типу char
        Check_Match(letter);
        Console.WriteLine(guessed_word);
        Console.WriteLine($"{letter}");
      }
    }
  }
}