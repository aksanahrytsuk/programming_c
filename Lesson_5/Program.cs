﻿using System;
using System.IO;

namespace Lesson_5
{
    class Program
    {
        static void Summ()
        {
            Console.WriteLine("Посчитаем сумму двух чисел.\n");
            Console.WriteLine("Введите первое число:");
            int numb1 = Convert.ToInt32(Console.ReadLine()); //всё, что вводит пользователь, передаётся в переменную numb1 с типом int
            Console.WriteLine("Введите второе число: ");
            int numb2 = Convert.ToInt32(Console.ReadLine());
            int summ = numb1 + numb2; //сумма вводимых чисел предается в переменную summ
            Console.WriteLine("Сумма двух чисел: " + summ);
        }
        static void Area_Rectangel()
        {
            Console.WriteLine("Вычеслим площадь прямоугольника.\n");
            Console.WriteLine("Введите длину прямоугольника:");
            int numb1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите ширину прямоугольника: ");
            int numb2 = Convert.ToInt32(Console.ReadLine());
            int area = numb1 * numb2;
            Console.WriteLine("Площадь прямоугольника равна: " + area);
        }

        static void Area_Circle() 
        {
            float p = 3.14f;
            Console.WriteLine("Вычеслим площадь круга.\n");
            Console.WriteLine("Введите радиус круга:");
            float r_circle = Convert.ToInt32(Console.ReadLine());
            float area_circle = p * r_circle * r_circle;
            Console.WriteLine("Площадь круга равна: " + area_circle);
        }

        static void Even_Odd()
        {
            Console.WriteLine("Определим четность числа.\n");
            Console.WriteLine("Введите число: ");
            int numb = Convert.ToInt32(Console.ReadLine());
            if (numb % 2 == 0) // если остаток от деления равен 0
            {
                Console.WriteLine("Ваше число " + numb + " четное");
            }
            else
            {
                Console.WriteLine("Ваше число " + numb + " нечетное");
            }
                   
        }

        static void Summ_Array()
        {
            Console.WriteLine("Введите числа через пробел:");
            string arr_numb =  Console.ReadLine(); // считывает строку, всё что введено передаётся в arr_numb с типом string 
            string[] arr_string = arr_numb.Split(' '); // разделяет строку по пробелам и переводит в массив строк 
            int summ = 0;

            foreach (string num in arr_string) //определяем переменную num для каждого элемента массива arr_string
            {
                summ += Convert.ToInt32(num);
            }
            Console.WriteLine("Сумма чисел в массиве равна: " + summ);
        }
        public static void Main1(string[] args)
        {
            string align = "\n=================================================== \n";
            Console.WriteLine("Hello");
            
            //Summ();
            Console.WriteLine(align);
            //Area_Rectangel();
            Console.WriteLine(align);
            //Area_Circle();
            Console.WriteLine(align);
            //Even_Odd();
            Console.WriteLine(align);
            //Summ_Array();

            string path = @"C:\Users\Оксана\source\repos\Lesson_5\Lesson_5\word_rus.txt"; //передаем путь к файлу со списком слов в переменную path
            string[] wordsList = File.ReadAllLines(path); 
            Random random = new Random();

            while (true) // основной игровой цикл
            {
                string word = wordsList[random.Next(0, wordsList.Length)]; //в переменной word лежит загаданное слово
                char[] viewWord = new char[word.Length]; 

                for (int i = 0; i < viewWord.Length; i++ )
                {
                    viewWord[i] = '-';
                }

                Console.WriteLine($"Загадано слово из {word.Length} букв");

                int opennedLetters = 0;
                int errorCount = 7;
               
                while (errorCount > 0 && opennedLetters != word.Length)
                {
                    Console.WriteLine("Введите букву");
                    string inputString = Console.ReadLine();

                    if (inputString.Length == 0 || !Char.IsLetter(inputString[0]))
                    {
                        Console.WriteLine("Вводите только одну букву");
                        continue;
                    }
                    if (!Char.IsLetter(inputString[0]))
                    {
                        Console.WriteLine("Вводи только буквы");
                       // continue; 
                    }

                    bool isLetterExist = false;

                    for (int i = 0; i < word.Length; i++)
                    {
                        if (viewWord[i] != '_' && word[i] == inputString[0])
                        {
                            viewWord[i] = inputString[0];
                            opennedLetters++;
                        }
                    }
                    if(isLetterExist)
                    {
                        Console.WriteLine("Угадал!");
                    }
                    else
                    {
                        Console.WriteLine("Такой буквы нет.");
                        errorCount--;
                    }

                    Console.WriteLine(viewWord);

                }

                if (errorCount == 0)
                {
                    Console.WriteLine ($"Ты проиграл! Слово {word}");
                }
                else
                {
                    Console.WriteLine("Молодец, ты выиграл!");
                }

                Console.WriteLine("Хочешь еще сыграть?[да / нет]");

                string answer = Console.ReadLine();
                if(answer != "нет")
                {
                    break;
                }
            }
        }
    }
}
