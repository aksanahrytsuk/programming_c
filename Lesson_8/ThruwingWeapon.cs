
using System;
namespace Lesson_8
{
  class ThrowingWeapon : Weapon
  {
    protected float damage = 1f;
    protected float distanceToObject = 1f;
    protected float throwForce = 5f;
    protected float reloadTimeToThrow = 5f;
    protected string weaponName = "";

    public ThrowingWeapon()
    {

    }

    public ThrowingWeapon(string weaponName)
    {
      this.weaponName = weaponName;
    }

    public virtual void Weapon_Info()
    {
      Console.WriteLine($"Name: {weaponName} \n ");
    }

    public virtual void Do_Damage(float damage) => this.damage = damage;
  }
}