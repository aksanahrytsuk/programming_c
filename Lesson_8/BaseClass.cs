﻿using System;
namespace Lesson_8
{
  class BaseClass
  {
    protected string name;
    protected int health = 10;
    protected int maxHealth = 10;
    protected int speed = 10;
    protected const float MINSPEED = 0;

    public BaseClass(string name)
    {
      this.name = name;
    }

    public BaseClass()
    {
    }
    public virtual void Info()
    {
      Console.WriteLine($"Name: {name} \n {health}");
    }
  }
}

