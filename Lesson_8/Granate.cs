using System.Collections.Generic;
namespace Lesson_8
{
  class Granate : ThrowingWeapon
  {
    private float explosiveRadius { get; set; }

    List<Player> playersList = new List<Player>();
    public Granate(string weapon) : base(weapon)
    {
    }

    // при попадании в плеера
    public override void Do_Damage(float damage)
    {
      base.Do_Damage(damage);
      CritDamage();
    }

    void CritDamage()
    {
      // убивает сразу
    }

    public void Do_Damage(float damage, float explosiveRadius)
    {
      // урон по радиусу
    }
  }
}
