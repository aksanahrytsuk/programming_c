﻿using System;
using System.Collections.Generic;

namespace Lesson_8
{
  class Player_Doctor : Player
  {
    public int rank = 1;

    Weapon weapon2;

    public Player_Doctor()
    {
    }

    public Player_Doctor(string name, string callSing, int rank) : base(name, callSing)
    {
      this.rank = rank;

      this.callSing = callSing;
    }

    public override void Info()
    {
      base.Info();
    }

    private void Heal(Player player)
    {
    }
  }
}

