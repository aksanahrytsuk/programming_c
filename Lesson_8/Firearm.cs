
namespace Lesson_8
{
  class Firearm : Weapon
  {
    //protected int bullets = 10;
    protected int maxBulletsCount = 0;
    protected float reloadTimeToBullet = 5f;
    protected float changeStateTime = 5f;
    protected float shootDelay = 0.1f;
    protected float throwForce = 5f;

    public Firearm()
    {

    }
  }
}