﻿using System;

namespace Lesson_8
{
  class Player : BaseClass
  {
    public string callSing = "";
    Weapon weapon2;

    public Player()
    {
    }

    public Player(string name, string callSing) : base(name)
    {
      this.callSing = callSing;
    }

    public void Run()
    { 
    }

    public override void Info()
    {
      base.Info();
    }

    public virtual void Change_Weapon(Weapon weapon)
    {
      this.weapon2 = weapon;
    }
  }
}

