﻿using System;
using System.Collections.Generic;

namespace Lesson_8
{
  class Player_Engineer : Player
  {
    public int rank = 1;

    Weapon weapon2;

    public Player_Engineer()
    {
    }

    public Player_Engineer(string name, string callSing, int rank, Weapon weapon) : base(name, callSing)
    {
      this.rank = rank;
      this.weapon2 = weapon;
    }

    public override void Info()
    {
      base.Info();
    }

    private void Build_Thurel()
    {
    }
  }
}

