﻿using System;
using System.Collections.Generic;

namespace Lesson_10_Class_Work
{
    class Program
    {
        public static void Main(string[] args)
        {
            Action action = null;

            action?.Invoke();
        }

        public static void Method1()
        {
            Console.WriteLine("Call M1");
            throw new ArgumentException ("Тестовое исключение.");
        }
         public static void Method2()
        {
            Console.WriteLine("Call M1");
        }
         public static void Method3()
        {
            Console.WriteLine("Call M1");
        }

    }

    public class Player
    {
        private Health _health;
        private bool _isAlive;

        // public Player
        // {
        //     _health.OnDie += Dead;
        // }

        // public void Dead()
        // {
        //     _isAlive = false;
        //     _health.OnDie -= Dead;
        // }
    }

    public class Health
    {
        private float _value;
        public event Action OnDie;
        public float Value => _value;

        public Health (float startValue)
        { 
            if (startValue < 0) 
            {
                throw new ArgumentException("startValue не должен быть < 0!");
            }
            _value = startValue;
        }

        public void Decrease(float damage)
        {
            _value-=damage;
            if (_value <= 0)
            {
                OnDie?.Invoke();
            }
        }
    }
}
