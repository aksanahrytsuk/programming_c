﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_7
{
    class Person
    {
        public string Name;
        public string Surename;
        public int Age;

        public Person(string name, string surename, int age)
        {
            Name = name;
            Surename = surename;
            Age = age;
        }

        public void ChangeName(string name) => Name = name; //метод для изменения

    }
}
