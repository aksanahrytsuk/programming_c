using System;
namespace Lesson_7
{
  public class GeneralClass
  {
    public static void Main(string[] args)
    {
      Bank bank = new Bank("Банк для тебя", "Косой переулок");
      while (true)
      {
        Console.WriteLine("Выбрать операцию:\n1.Добавить клиента.\n2.Информация о клиенте по его имени.\n3.Удаление клиента.\n4.!!!Удалить всех клиентов.\n5.Информация о всех клментах в алфавитном порядке.\n");
        int number = Convert.ToInt32(Console.ReadLine());
        switch (number)
        {
          case 1:
            bank.AddClients();
            break;

          case 2:
            bank.ShowClient();
            break;

          case 3:
            bank.RemoveClientFromList();
            break;

          case 4:
            bank.RemoveAllClients();
            break;

          case 5:
            bank.ShowAllClientsABC();
            break;
        }
      }
    }
  }
}