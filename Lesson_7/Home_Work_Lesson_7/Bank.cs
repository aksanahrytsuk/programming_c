using System;
using System.Collections.Generic;
namespace Lesson_7
{
  public class Bank
  {
    public string BankName;
    public string BankAdress;
    List<Clients> clientsList = new List<Clients>(); // список клиентов ввиде экземляра Clients
    public Bank() //конструктор по умолчанию
    {
      BankName = "Банк для тебя";
      BankAdress = "Косой переулок";
    }

    public Bank(string bankName, string bankAdress)
    {
      BankName = bankName;
      BankAdress = bankAdress;
    }

    public void AddClients()
    {
      Console.WriteLine("Введите количество клиентов.");
      int count = Convert.ToInt32(Console.ReadLine());
      for (int i = 0; i < count; i++)
      {
        Console.WriteLine("Введите имя клиента: ");
        string name = Console.ReadLine();
        Console.WriteLine("Введите фамилию клиента: ");
        string sureName = Console.ReadLine();
        Console.WriteLine("Введите возраст клиента: ");
        int age = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Введите баланс клиента: ");
        int balance = Convert.ToInt32(Console.ReadLine());

        Clients clients = new Clients(name, sureName, age, balance); //вызов коструктора Сlients с 4мя параметрами. Создание экземпляра класса клиентов
        clientsList.Add(clients); // add client in ClientsList
      }
    }
    public void ShowClient()
    {
      Console.WriteLine("Введите имя клиента, чтобы получить информацию");
      string searchingClients = Console.ReadLine();
      for (int i = 0; i < clientsList.Count; i++)
      {
        if (searchingClients == clientsList[i].Name)
        {
          clientsList[i].ShowClientInfo();
        }
       
      }
    }

    public void RemoveClientFromList()
    {
      Console.WriteLine("Введите имя клиента");
      string searchingClients = Console.ReadLine();
      for (int i1 = 0; i1 < clientsList.Count; i1++)
      {
        if (searchingClients == clientsList[i1].Name)
        {
          clientsList.Remove(clientsList[i1]);
        }
      }
    }

    public void RemoveAllClients()
    {
      int count = Convert.ToInt32(Console.ReadLine());
      for (int i = 0; i < count; i++)
      {
        string name = Console.ReadLine();
        string sureName = Console.ReadLine();
        int age = Convert.ToInt32(Console.ReadLine());
        int balance = Convert.ToInt32(Console.ReadLine());

        Clients clients = new Clients(name, sureName, age, balance); //вызов коструктора Сlients с 4мя параметрами.
        clientsList.Remove(clients);                                                         //метод удаления клиентов
      }
    }

    public void ShowAllClientsABC()
    {
      clientsList.Sort();

      foreach (Clients client in clientsList)
      {
        Console.WriteLine($"Список клиентов: {client.Name}");
      }
    }
     public void Exit()
  {
     
  }
  }
}



