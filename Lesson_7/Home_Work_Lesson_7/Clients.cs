using System;
namespace Lesson_7
{
 public class Clients
  {
    public string Name;
    public string SureName;
    public int Age;
    public int Balance;

    public Clients() //конструктор по умолчанию
    {
      Name = "Alex";
      SureName = "Signal";
      Age = 25;
      Balance = 100;
    }

    public Clients(string name, string sureName, int age, int balance)
    {
      Name = name;
      SureName = sureName;
      Age = age;
      Balance = balance;

      if (age < 0)
      {
        Console.WriteLine("Возраст не может иметь отрицательное значение!");
      }
    }

    //public void ReName(string name) => Name = name; // метод для изменения имени
    //public void ReSureName(string sureName) => SureName = sureName; // метод для изменения фамилии
    //public void ReAge(int age) => Age = age;
    //public void ReBalance(float balance) => Balance = balance;

    public void ShowClientInfo()
    {
        Console.WriteLine($"Имя: {Name} \n  Фамилия: {SureName} \n  Возраст: {Age}\n  Балланс: {Balance}\n");
    }
  }
}