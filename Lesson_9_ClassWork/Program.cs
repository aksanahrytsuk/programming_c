﻿using System;

namespace Lesson_9_ClassWork
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person ("Misha", "Ivanov", 30);
            Person person1 = new Person("Pavel", "Sidorov", 20);
        }
    }

    class Person
    {
        private string name;
        private string sureName;
        private int age;

        public string Name => name; // access to read only
        public string SureName => sureName; // access to read only
        public int Age => age; // access  to read only

        public Person ( string name , string surename, int age)
        {
            this.name = name;
            this.sureName = surename;
            this.age = age;
        }
         public override string ToString()
         {
             return $"{Name}, {sureName}, {age}";
         }
    }
}
