using System;

namespace Lesson_9_HomeWork
{
  abstract class Eatable : Items
  {
    protected float _productWeight;
    protected string _manufactureddate;
    protected string _expirationdate;
    protected float _callories;

    public float ProductWeight
    {
      get { return _productWeight; }
    }
    public string Manufactureddate => _manufactureddate;
    public string Expirationdate => _expirationdate;
    public float Callories { get => _callories; }

    public Eatable()
    {
      _productWeight = 5f;
      _manufactureddate = "10-04-22";
      _expirationdate = "10 суток";
      _callories = 1f;
    }
   
    public override void ShowItemsInfo()
    {
      base.ShowItemsInfo();
      Console.WriteLine($"Фасавка/граммы: {ProductWeight} грамм \nВ 100 граммах продукта: {Callories} калорий  \nДата изготовления: {_manufactureddate}  \nСрок реализации: {_expirationdate}");
    }
  }
}
