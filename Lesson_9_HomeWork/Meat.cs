namespace Lesson_9_HomeWork
{
    class Meat : Eatable
    {
        public Meat()
        {
            _productname = "Мясо кролика";
            _price = 10;
            _amount = 1;
            _manufactureddate = "10-04-22";
            _expirationdate = "3 суток";
            _productWeight = 1000;
            _callories = 100;
        }
    }
}