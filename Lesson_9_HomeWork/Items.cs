using System;
namespace Lesson_9_HomeWork
{
  abstract class Items
  {
    protected string _information;
    protected string _productname;
    protected float _price;
    protected float _amount;

    public string Information => _information;
    public string ProductName => _productname;
    public float Price => _price;
    public float Amount => _amount;

    public Items()
    {
      _information = "Иформация:";
      _productname = "Something";
      _price = 10f;
      _amount = 10f;
    }

    public virtual void ShowItemsInfo()
    {
      Console.WriteLine("__________________");
      Console.WriteLine($"Наименование: {ProductName} \nЦена: {Price} \nКоличество: {Amount}");
    }

    public override string ToString()
    {
      return $"Продукт:  {ProductName}  \nЦена:   {Price}  /nКоличество: {Amount}";
    }
  }
}