namespace Lesson_9_HomeWork
{
    class Potato : Eatable, IVegetarian
    {
        public Potato()
        {
            _productname = "Картошка молодая";
            _price = 10;
            _amount = 1;
            _manufactureddate = "10-04-22";
            _expirationdate = "20 суток";
            _productWeight = 1000;
            _callories = 200;
        }
    }
}