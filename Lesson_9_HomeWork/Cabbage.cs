namespace Lesson_9_HomeWork
{
    class Cabbage : Eatable, IVegetarian
    {
        public Cabbage()
        {
            _productname = "Капуста молодая";
            _price = 2;
            _amount = 1;
            _manufactureddate = "10-04-22";
            _expirationdate = "20 суток";
            _productWeight = 1000;
            _callories = 50;
        }
    }
}