﻿using System;
namespace Lesson_9_HomeWork
{
  class Program
  {
    static void Main(string[] args)
    {
      Grocery grocery = new Grocery("Mall");
      grocery.AddItems(new Meat());
      grocery.AddItems(new Potato());
      grocery.AddItems(new Cabbage());
      grocery.AddItems(new Books());

      // Products list with information 
      foreach (Items item in grocery.ItemsList)
      {
        Console.WriteLine(item.Information);
        item.ShowItemsInfo();
      }

      // Deleted Item by name
      Console.WriteLine("Введите название продукта, которые хотите удалить.");
      string _producttoremove = Console.ReadLine();
      grocery.RemoveItems(_producttoremove);

      // Show Eatable products
      for (int i = 0; i < grocery.ItemsList.Count; i++)
      {
        if (grocery.ItemsList[i] is Eatable)
        {
          Console.WriteLine(grocery.ItemsList[i] as Eatable);
        }
      }
    }
  }
}
