using System;
namespace Lesson_9_HomeWork
{
  class Books : UnEatable
  {
      protected string _pages;
      public string Pages
    {
        get { return _pages;}
    }
    public Books()
    {
      _productname = "Книга";
      _price = 20;
      _amount = 1;
      _pages = "216";
    }

    public override void ShowItemsInfo()
    {
      base.ShowItemsInfo();
      Console.WriteLine($"Количество страниц: {Pages}");
    }
  }
}