using System;
using System.Collections.Generic;
namespace Lesson_9_HomeWork
{
  class Grocery
  {
    protected string groceryName;
    public List<Items> ItemsList = new List<Items>();

    public Grocery(string groceryName)
    {
      this.groceryName = groceryName;
    }

    public string GroceryName
    {
      get { return groceryName; }
      set
      {
        if (value != null)
        {
          value = groceryName;
        }
        else
        {
          Console.WriteLine("Введите название магазина.");
        }
      }
    }

    public void AddItems(Items item)
    {
      Console.WriteLine("Введите количество продуктов.");
      int count = Convert.ToInt32(Console.ReadLine());
      for (int i = 0; i < count; i++)
      {
        ItemsList.Add(item);
      }
    }

    public void RemoveItems(string name)
    {
      foreach (Items items in ItemsList)
      {
        if (items.ProductName == name)
        {
          ItemsList.Remove(items);
        }
      }
    }

    public void SortItems()
    {
      Items temp;
      for (int i = 0; i < ItemsList.Count; i++)
      {
        for (int j = i + 1; j < ItemsList.Count; j++)
        {
          if (ItemsList[i].Price > ItemsList[j].Price)
          {
            temp = ItemsList[i];
            ItemsList[i] = ItemsList[j];
            ItemsList[j] = temp;
          }
        }
      }
      
    }

    public void CheckVegetarian()
    {
      for (int i = 0; i < ItemsList.Count; i++)
      {
        if (ItemsList[i] is IVegetarian)
        {
          Console.WriteLine(ItemsList[i] as IVegetarian);
        }
      }
    }
  }
}